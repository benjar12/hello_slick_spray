import AssemblyKeys._

version       := "0.1"

scalaVersion  := "2.11.2"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

libraryDependencies ++= {
  val akkaV = "2.3.5"
  val sprayV = "1.3.2"
  Seq(
    "io.spray"            %%  "spray-servlet" % sprayV,
    "io.spray"            %%  "spray-routing" % sprayV,
    "io.spray"            %%  "spray-can"     % sprayV,
    "io.spray"            %%  "spray-testkit" % sprayV % "test",
    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test",
    "org.specs2"          %%  "specs2-core"   % "2.3.11" % "test",
    "com.typesafe.slick"  %% "slick"          % "2.1.0",
    "org.slf4j"           % "slf4j-api"       % "1.7.5",
    "org.slf4j"           % "slf4j-simple"    % "1.7.5",
    "mysql"               % "mysql-connector-java" % "5.1.6",
    "com.typesafe"        % "config"          % "1.2.1",
    "org.scala-lang"      % "scala-reflect"   % "2.11.6",
    "io.spray" % "spray-json_2.11" % "1.3.1",
    "org.json4s" %% "json4s-native" % "3.2.10",
    "org.json4s" %% "json4s-jackson" % "3.2.11"
  )
}

mainClass in assembly := Some("Main")

assemblySettings

jetty()
