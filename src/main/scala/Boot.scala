/**
 * Created by benjarman on 4/14/15.
 */

import Util.Logging
import akka.actor.{Props, ActorSystem}
import spray.servlet.WebBoot

// This class is instantiated by the servlet initializer.
// It can either define a constructor with a single
// `javax.servlet.ServletContext` parameter or a
// default constructor.
// It must mplement the spray.servlet.WebBoot trait.
class Boot extends WebBoot {

  val logger = new Logging(classOf[Boot].getCanonicalName)

  // we need an ActorSystem to host our application in
  val system = ActorSystem("actorsystem")

  // the service actor replies to incoming HttpRequests
  val serviceActor = system.actorOf(Props[WebServiceActor])

  system.registerOnTermination {
    // put additional cleanup code here
    system.log.info("Application shut down")
  }
}