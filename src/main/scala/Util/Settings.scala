package Util

/**
 * Created by benjarman on 4/8/15.
 */
import com.typesafe.config.ConfigFactory

case class JdbcSettings(driver: String, connString: String)
case class WebServerSettings(inet: String, port: Int, context: String, version: String)

object Settings {
  private val conf = ConfigFactory.load()



  private def ifExistsOrDefault(path: String, default: String): String = {
    if(conf.hasPath(path))
      conf.getString(path)
    else
      default
  }

  private def ifExistsOrDefault(path: String, default: Int): Int = {
    if(conf.hasPath(path))
      conf.getInt(path)
    else
      default
  }

  def getDataBaseSettings(): JdbcSettings =
    JdbcSettings(
      ifExistsOrDefault("database.jdbc_driver", "com.mysql.jdbc.Driver"),
      ifExistsOrDefault("database.jdbc_url", "jdbc:mysql://localhost/test")
    )

  def getWebServerSettings(): WebServerSettings =
    WebServerSettings(
      ifExistsOrDefault("server.inet", "localhost"),
      ifExistsOrDefault("server.port", 9091),
      ifExistsOrDefault("server.context", "api"),
      ifExistsOrDefault("server.version", "v1")
    )

}
