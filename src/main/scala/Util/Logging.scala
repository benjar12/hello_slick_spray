package Util

/**
 * Created by benjarman on 4/10/15.
 */
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Logging(name: String){

  private [this] val logger = LoggerFactory.getLogger(name)

  logger.info("New Logger Created")

  def debug(msg: String){logger.debug(msg)}
  def info(msg: String){logger.info(msg)}
  def warn(msg: String){logger.warn(msg)}
  def error(msg: String){logger.error(msg)}
}
