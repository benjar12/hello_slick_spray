/**
 * Created by benjarman on 4/10/15.
 */
import Util.Settings
import models._
import scala.slick.driver.MySQLDriver.simple._

object CreateDDL {

  def main(args: Array[String]): Unit ={

    val dbSettings = Settings.getDataBaseSettings()
    val db = Database.forURL(dbSettings.connString, driver = dbSettings.driver)

    db withSession {
      implicit session =>
      try {
        TableQuery[UsersTable].ddl.create
      }catch {
        case e: Exception => println(e)
      }
    }
  }

}
