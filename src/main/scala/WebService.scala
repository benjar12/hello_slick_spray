/**
 * Created by benjarman on 4/8/15.
 */

import akka.actor.{Actor, ActorLogging}
import akka.io.IO
import akka.util.Timeout
import akka.actor._
import spray.can.Http
import spray.can.server.Stats
import spray.util._
import spray.http._
import HttpMethods._
import MediaTypes._
import spray.can.Http.RegisterChunkHandler
import scala.slick.driver.MySQLDriver.simple._
import scala.concurrent.duration._
import Util.{Logging, Settings}
import routes._

class WebService extends Actor with ActorLogging {

  val logger = new Logging(classOf[WebService].getCanonicalName)

  //get our webserver settings base path
  //will be something like /api
  val settings = Settings.getWebServerSettings()
  val basePath = settings.basePath

  implicit val timeout: Timeout = 1.second
  import context.dispatcher

  def actorRefFactory = context

  /*
  Override method for request routes
   */
  def receive = {
    case _: Http.Connected => sender ! Http.Register(self)

    case HttpRequest(GET, Uri.Path("/"), _, _, _) => {
      sender ! Root.get()
    }
      sender ! Root.get()

  }
}
