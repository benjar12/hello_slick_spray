/**
 * Created by benjarman on 4/14/15.
 */

import Util.{Logging, Settings}
import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http
/*
Use this to run the spray-can web server,
sbt run
 */
object Main extends App {

  val logger = new Logging("Main.scala")

  val settings = Settings.getWebServerSettings()

  implicit val system = ActorSystem()

  // the handler actor replies to incoming HttpRequests
  val handler = system.actorOf(Props[WebServiceActor], name = "WebServiceActor.scala")

  //bind to port in app context
  IO(Http) ! Http.Bind(handler, interface = settings.inet, port = settings.port)
}
