/**
 * Created by benjarman on 4/8/15.
 */

import akka.actor.{Actor, ActorLogging}
import akka.io.IO
import akka.util.Timeout
import akka.actor._
import spray.can.Http
import spray.can.server.Stats
import spray.http.Uri.Path.Segment
import spray.routing.HttpService
import spray.util._
import spray.http._
import HttpMethods._
import MediaTypes._
import spray.can.Http.RegisterChunkHandler
import scala.slick.driver.MySQLDriver.simple._
import scala.concurrent.duration._
import Util.{Logging, Settings}
import routes._

class WebServiceActor extends Actor with ActorLogging with WebService {

  val logger = new Logging(classOf[WebServiceActor].getCanonicalName)

  logger.info(s"Context: $scontext Version: $sversion")

  implicit val timeout: Timeout = 1.second
  import context.dispatcher

  def actorRefFactory = context

  /*
  Override method
   */
  def receive = runRoute(webRoute)
  /*def receive = {
    case _: Http.Connected => sender ! Http.Register(self)

    case HttpRequest(GET, Uri.Path("/"), _, _, _) => {
      sender ! Root.get()
    }
    case HttpRequest(GET, Uri.Path("/user" / "id" ~ Segment), _, _, _) => {
      path(""/ "id" ~ Segment)
      sender ! Root.get()
    }

  }*/
}

trait WebService extends HttpService with Root with UserRoute {

  implicit def executionContext = actorRefFactory.dispatcher

  val webRoute = {
      rootRouting ~
      userRouting
  }
}
