package routes

import Util.Settings

/**
 * Created by benjarman on 4/14/15.
 */
trait BaseRoute {

  //get our webserver settings base path
  //will be something like /api
  val settings = Settings.getWebServerSettings()
  def scontext = settings.context
  def sversion = settings.version

}
