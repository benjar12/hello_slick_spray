package routes

import dao.UserDao
import Util._
import spray.http.MediaTypes._
import spray.http._
import spray.routing.HttpService

import org.json4s.native.Serialization.write
import org.json4s.DefaultFormats

/**
 * Created by benjarman on 4/14/15.
 */
trait User extends HttpService with BaseRoute{

  private val dao = new UserDao()

  val userRouting = {
    path(scontext / sversion / "user" / Segment) { id => {
      get {
          val u = dao.findById(id.toInt)
          implicit val formats = DefaultFormats
          complete( write( u ) )
        }
      }

    }
  }
}
