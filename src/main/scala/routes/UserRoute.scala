package routes

import dao.UserDao
import Util._
import models._
import org.json4s.JsonAST.JObject
import spray.http.MediaTypes._
import spray.http._
import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol
import spray.routing.HttpService

import org.json4s.native.Serialization.write
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods._

/**
 * Created by benjarman on 4/14/15.
 */
trait UserRoute extends HttpService with BaseRoute{

  private val dao = new UserDao()

  import UserJsonSupport._
  implicit val formats = DefaultFormats

  val userRouting = {
    path(scontext / sversion / "user" / Segment) { id => {
      get {
          val u = dao.findById(id.toInt)
          complete( write( u ) )
        }
      } ~
      post{
        entity(as[User]){ updates =>

          val u = dao.findById(id.toInt)
          complete( write( updates ) )

        }
      }

    }
  }
}


