package routes

import akka.actor.SupervisorStrategy.Directive
import Util.Logging
import shapeless.HNil
import spray.http.HttpHeaders.`Content-Type`

import spray.http._
import MediaTypes._
import spray.httpx.marshalling.ToResponseMarshallable
import spray.routing.{PathMatcher, StandardRoute, HttpService, Route}

/**
 * Created by benjarman on 4/14/15.
 */
trait Root extends HttpService with BaseRoute{

  private lazy val index =
      <html>
        <body>
          <h1>TODO : Supply Docs</h1>
        </body>
      </html>.toString()


  def rootRouting = {
    path(scontext / sversion){
      get{
        respondWithMediaType(`text/html`) {
          complete(200, index)
        }
      }
    }
  }
}
