package dao

import Util._
import models._
import org.json4s.jackson.JsonMethods._


import scala.slick.driver.MySQLDriver.simple._
import scala.slick.jdbc.JdbcBackend
import scala.slick.lifted.TableQuery

/**
 * Created by benjarman on 4/9/15.
 */

abstract class BaseDao[T <: Table[E] with IdentifiableTable[PK], E <: Entity[PK] with Copying[E], PK: BaseColumnType, AC] {

  //type TP = TPI

  def logger: Logging
  def tableReference: TableQuery[T]

  private val dbSettings = Settings.getDataBaseSettings()
  private val db = Database.forURL(dbSettings.connString, driver = dbSettings.driver)

  /*
  A simple method for getting a Model case class,
  This way we only have to edit in one place when adding fields.
  This method will need to be overwritten by each object that
  extends this one.
   */
  def getColumns(u: T): AC

  /*
  Default criteria always returns true, thus grabbing everything.
  This criteria business is actually really cool. Because of the
  way that slick turning the code into sql i can have a very generic
  function pattern that when a really specific function gets passed in
  is turned into SQL statements.
   */
  private def defaultCriteria(scy: AC): Boolean = { true }

  /*
  Args: criteria: A function that takes in a instance of a caseClass and produces Boolean
        limit: The number of rows to keep
   */
  def find(
            criteria: (AC) => Column[Boolean]=defaultCriteria,
            limit: Int=0
            ): List[E] ={
    db withSession {
      implicit session =>
        val dat = for {
          u <- tableReference if criteria( getColumns(u) )
        } yield (u)
        val length = dat.size
        logger.info(s"Found $length records")
        if(limit != 0)
          dat.take(limit).list
        else
          dat.list
    }
  }

  /*
  Finds the first match
   */
  def findOne(criteria: (AC) => Column[Boolean]=defaultCriteria): Option[E] = {
    logger.info("Finding one by criteria")
    find(criteria, 1).headOption
  }

  def findAll: List[E] = {
    logger.info(s"Finding all Records")
    db withSession {
      implicit session =>
        tableReference.list
    }
  }

  def findById(id: PK) = {
    db withSession {
      implicit session => queryById(id).list.head
    }
  }

  def queryById(id: PK) = {
    logger.info(s"Finding record by ID: $id")
    db withSession {
      implicit session => tableReference.filter(_.id === id)
    }
  }

  def deleteById(id: PK): Unit = {
    logger.info(s"Deleting record by ID: $id")
    db withSession {
      implicit session => queryById(id).delete
    }
  }

  def create(cc: E): Unit ={
    logger.info(s"Creating record: $cc")
    db withSession {
      implicit session =>
        //tableReference.ddl.create
        tableReference += (cc)
    }
  }


  /*
  This takes two case classes and replaces values in
  old with values inside new. We ignore None type because
  that would not work for optional fields. This is where
  the copying trait is used
   */
  def mergeUpdate(newCC: E, oldCC: E): E = {

    val fields = (Map[String, Any]() /: newCC.getClass.getDeclaredFields) { (a, f) =>
      f.setAccessible(true)
      a + (f.getName -> f.get(newCC))
    }

    fields.foldRight(oldCC)( (r, n)=>{
      r._2 match {
        case None => n
        case _    => n.copyWith( ( r._1, r._2.asInstanceOf[AnyRef] ) )
      }
    })

  }

  def update(id: PK, cc: E): Unit ={
    logger.info(s"Updating record by ID: $id")
    db withSession {
      implicit session =>
        val q = queryById(id).list.head
        queryById(id).update( mergeUpdate(cc, q) )
    }
  }




}

