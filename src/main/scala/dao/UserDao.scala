package dao

import models._
import Util.Logging

import scala.slick.lifted.{TableQuery}
import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by benjarman on 4/9/15.
 */
class UserDao extends BaseDao[UsersTable, User, Int, UserColumns]{

  /*
  Override Val
   */
  def logger = new Logging(classOf[UserDao].getCanonicalName)
  def tableReference = TableQuery[UsersTable]

  /*
  Overrides the BaseDao method for getting the columns
   */
  def getColumns(u: UsersTable): UserColumns ={
    UserColumns.tupled(u.getTuple())
  }

  /*
  Additional methods that are specific to this model
   */

  def findByFirstName(name: String): List[User] ={

    def matches(us: UserColumns): Column[Boolean] = {
      us.fName.getOrElse("") === name
    }

    find(matches)

  }


}
