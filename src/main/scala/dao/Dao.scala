package dao

/**
 * Created by benjarman on 4/9/15.
 */
import scala.reflect._
import scala.slick.driver.MySQLDriver.simple._
import Util.Settings
import models._

import scala.slick.util.CloseableIterator
/*
class Dao() {

  private val dbSettings = Settings.getDataBaseSettings()
  private val db = Database.forURL(dbSettings.connString, driver = dbSettings.driver)


  def createUser(firstName: String, lastName: String): Unit ={
    db withSession {
      implicit session =>
        //we set id to -1 because it is not needed during creation
        val me = User(-1, firstName, lastName)
        val users = TableQuery[UsersTable]
        users.ddl.create
        users += (me)
    }
  }

  /*
  A simple method for getting a user case class,
  This way we only have to edit in one place when adding fields
   */
  private def getUserColumns(u: UsersTable): UserColumns ={
    UserColumns(u.id, u.fName, u.lName)
  }

  /*
  Default criteria always returns true, thus grabbing everything
   */
  private def defaultCriteria(user: UserColumns): Boolean = { true }

  def findUsers(criteria: (UserColumns) => Boolean=defaultCriteria): List[User] ={
    db withSession {
      implicit session =>
        val users = TableQuery[UsersTable]
        val dat = for {
          u <- users if criteria(getUserColumns(u))
        } yield (u)
        dat.list
    }
  }

}*/