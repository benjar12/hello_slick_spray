package models

/**
 * Created by benjarman on 4/8/15.
 */

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

import scala.slick.driver.MySQLDriver.simple._
import scala.slick.lifted.{ProvenShape}
import scala.reflect.runtime.{universe => ru}
/*
I Really dont like all this duplication but until i figure out a
better way its here to stay.
 */
case class User(id: Int, email: String, fName: Option[String]=None, lName: Option[String]=None) extends Entity[Int] with Copying[User]
case class UserColumns(id: Column[Int], email: Column[String], fName: Column[Option[String]]=None, lName: Column[Option[String]]=None)

object UserJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val PortofolioFormats = jsonFormat4(User)
}

class UsersTable(tag: Tag)
  extends Table[User](tag, "Users")
  with IdentifiableTable[Int]
{
  def id = column[Int]("USER_ID", O.PrimaryKey, O.AutoInc)
  def email = column[String]("EMAIL", O.NotNull)
  def fName = column[Option[String]]("FIRST_NAME", O.Nullable)
  def lName  = column[Option[String]]("LAST_NAME", O.Nullable)

  /*
  This method makes it so in the UserDao I don't have to edit the get columns
  method every time i add a new column. It overrides the method in the trait
  that is also passed to BaseDao making life super cool.
   */
  def getTuple():(Column[Int], Column[String], Column[Option[String]], Column[Option[String]]) ={
    (id, email, fName, lName)
  }

  def * = getTuple() <> (User.tupled, User.unapply)
}
